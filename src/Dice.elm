module Dice exposing (..)

import Random

type Face
    = Ace
    | King
    | Queen
    | Jack
    | Nine
    | Eight

type alias Score =
    Int

init : Face
init =
    Ace

toString : Face -> String
toString face =
    case face of
        Ace ->
            -- "⚀"
            "🂡"

        King ->
            -- "⚁"
            "🂮"

        Queen ->
            -- "⚂"
            "🂭"

        Jack ->
            -- "⚃"
            "🂫"

        Nine ->
            -- "⚄"
            "🂩"

        Eight ->
            -- "⚅"
            "🂨"

roll : Random.Generator Face
roll =
    Random.uniform Ace [ King, Queen, Jack, Nine, Eight ]

toScore : Face -> Score
toScore face =
    case face of
        Ace -> 10
        King -> 5
        _ -> 0


